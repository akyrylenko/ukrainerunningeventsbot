# coding UTF-8

dir_name = File.expand_path(File.dirname(__FILE__))

pid = fork do
  puts "#{dir_name}/main.rb"
  test_command = false
  if test_command
    exec 'ls -lA'
  else
    exec "ruby #{dir_name}/main.rb > log/bot.log"
  end
end
Process.detach(pid)
pid = pid.to_i + 4
puts "ProcessID: #{pid}"
File.open("#{dir_name}/bot_process.pid", 'w') { |file| file.write(pid.to_s) }
