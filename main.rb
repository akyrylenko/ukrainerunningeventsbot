# coding UTF-8

require 'telegram/bot'
require 'net/http'

UKRAINE_RUNNING_EVENTS_BOT_TOKEN = '1102803759:AAFaDVM8kf8kHPBvPd4Nv4HQT5ji5JpKhFI'
NEW_VIKING_BOT_TOKEN = '534764216:AAE-g3q4k-jAhIEmgP5xXMqg0ug4Plmc82w'
MAIN_CHAT_NAME = '@UkraineRunningEvents #ЗабігиУкраїнаБіжить'
IGNORE_COMMAND_TEXT = "Ця команда не може бути оброблена.\nСписок команд доступний за командою /info"
MESSAGE_PROCESSING_ERROR = 'помилка обробки повідомлення'
MESSAGE_SENDING_ERROR = 'помилка відправки повідомлення'
UNKNOWN_COMMAND_TEXT = 'Невідома команда'
HELP_TEXT = "Для отримання списку команд напишіть боту \/help"
BOT_ADMIN = 478257420
UKRAINE_RUNNING_ADMIN = 271206883
UKRAINE_RUNNING_EVENTS_HISTORY_ID = -1001415070558
TEST_VIKING_BOT_GROUP_NEW_ID = -379893045
IMAGE_LIST_START = '---IMAGE-LIST-START---'
IMAGE_LIST_ITEM = '---IMAGE-LIST-ITEM---'
IMAGE_LIST_END = '---IMAGE-LIST-END---'
EVENT_COMMANDS_REPLACEMENTS_RULE_SEPARATOR = '---COMMAND-REPLACEMENT-UA---'
EVENT_COMMANDS_REPLACEMENTS_ROW_SEPARATOR = '---EVENT-COMMAND-REPLACEMENT-ROW---'
EVENT_COMMANDS_REPLACEMENTS_TEXT_SEPARATOR = '---EVENT-COMMANDS-REPLACEMENTS---'
LAST_CHARACTERS_IN_LONG_MESSAGE = ' ---'
END_ROW_CHARACTERS_IN_LONG_MESSAGE = ';;; '
END_ROW_CHARACTERS_IN_LONG_MESSAGE_SHIFT = 2
MESSAGE_TEXT_IN_LOG_SIZE = 50
TELEGRAM_MAX_MESSAGE_TEXT_SIZE = 4000
MESSAGE_ENCODING_NAME = 'UTF-8'
SLEEP_TIME_IN_SECONDS_AFTER_EXCEPTION = 60 # wait 1 minute and restart
DEBUG_KEYWORD = 'debug'
FULLTEXT_KEYWORD = 'fulltext'
TODAY_KEYWORDS = ['today', 'сьогодні', 'сегодня']
YESTERDAY_KEYWORDS = ['yesterday', 'вчора', 'вчера', 'учора']
ID_KEYWORD = 'withid'
UKRAINE_CODE = 'ua'
BILORUS_CODE = 'by'
RUSSIA_CODE = 'ru'

INITIALIZING_TEXT = 'Initializing...'
STARTED_LISTENING_TEXT = 'Started listening...'
PARSE_MESSAGE_TEXT = 'Parse message:'
SEND_MESSAGE_BY_PARTIAL = 'SEND_MESSAGE_BY_PARTIAL'
SKIP_LONG_MESSAGE = 'SKIP LONG MESSAGE'
FORWARD_MESSAGE_TEXT = 'FORWARD MESSAGE'
OTHER_MESSAGE_TEXT = 'UNKNOWN else'
PHOTO_DIR_NAME = 'photos'

puts(INITIALIZING_TEXT)

stop_signal = 0
shutdown_command_received = false

REGISTER_API_URL = 'http://running.local:3030/'

class UkraineRunningEventsParser
  attr_reader :message
  attr_reader :bot
  attr_reader :test_bot
  attr_accessor :country
  attr_accessor :withid
  attr_accessor :date
  attr_reader :api_key
  attr_reader :tracked_message_ids
  def initialize(message, bot, test_bot=false)
    @api_key = UkraineRunningEventsParser.bot_token(test_bot)
    @message = message
    @bot = bot
    @test_bot = test_bot
    @language = message.from.language_code
    puts 'UkraineRunningEventsParser new message' 

    if bot_admin? && test_bot
      log([Time.now.to_s, PARSE_MESSAGE_TEXT, message.inspect].join(' ') + message_inspect)
    else
      log([Time.now.to_s, PARSE_MESSAGE_TEXT, message_inspect].join(' '))
    end
  end

  def self.bot_token(test_bot=false)
    if test_bot
      NEW_VIKING_BOT_TOKEN
    else
      UKRAINE_RUNNING_EVENTS_BOT_TOKEN
    end
  end

  def message_inspect(full_message=false)
    UkraineRunningEventsParser.message_inspect(message, (full_message || test_bot))
  end

  def self.message_inspect(message, full_message=false)
    skip_message = skip_message?(message)
    inspect_result = [
      PARSE_MESSAGE_TEXT,
      (skip_message ? "SKIP #{message.inspect}" : full_message ? message.text.to_s : message.text.to_s[0..50])
    ]
    inspect_result += if !skip_message && message.from
      [
        'from',
        telegram_sender_id(message),
        message.from.first_name,
        message.from.last_name,
      ]
    else
      []
    end
    inspect_result += if !skip_message && message.from
      [
        'chatID',
        message.chat.id,
        ':',
        message.chat.title
      ]
    else
      []
    end
    inspect_result.join(' ')
  end

  def format_name(string_name)
    (string_name || '').split('*').join('.').split('_').join('-').strip
  end

  def message_from
    message.from rescue nil
  end

  def super_admins_list
    [
      BOT_ADMIN
    ].compact.map &:to_s
  end

  def admins_list
    [
      BOT_ADMIN,
      UKRAINE_RUNNING_ADMIN
    ].compact.map &:to_s
  end

  def bot_admin?
    message && !skip_message? && message.from && super_admins_list.include?(telegram_sender_id.to_s)
  end

  def bot_admins?
    admins_list.include?(telegram_sender_id.to_s)
  end

  def message_chat_id
    message.chat.id
  end

  def file_for_photo(file_path)
    Faraday::UploadIO.new(file_path, 'photo')
  end

  def send_photo(file_name, file_path, file_caption='')
    if file_path && message_chat_id
      bot.api.send_photo(chat_id: message_chat_id, photo: file_for_photo(file_path), caption: file_caption.to_s)
    end
  end

  def chat_user_params
    {
      'chat[telegram_id]': telegram_sender_id,
      'chat[chat_telegram_id]': message_chat_id.to_s,
      'chat[telegram_time]': message.date,
      'chat[country]': self.country,
      'chat[withid]': self.withid,
    }
  end

  def chat_params
    {
      'chat[telegram_id]': message_chat_id.to_s,
      'chat[title]': message.chat.title,
      'chat[username]': message.chat.username,
      'chat[country]': self.country,
      'chat[withid]': self.withid,
    }
  end

  def event_bot_users_url(action_name)
    "event_bot_users/#{ action_name }"
  end

  def register_user
    url = event_bot_users_url(:register)
    params = user_params
    info_request(url, params, true)
  end

  def user_params
    {
      'user[first_name]': message.from.first_name,
      'user[last_name]': message.from.last_name,
      'user[username]': message.from.username,
      'user[telegram_id]': telegram_sender_id,
    }
  end

  def event_bot_user_url(action_name)
    "event_bot_user/#{ action_name }"
  end

  def bot_admin_help?(message_text)
    message_text == '/adminhelp'
  end

  def bot_admin_help(message_text=nil)
    begin
      (self.admin_help || 'Bot Admin help')
    rescue
      'Bot Admin help'
    end
  end

  def info_param(name)
    "help[#{name}]"
  end
  
  def info_params
    {
      'help[telegram_id]': telegram_sender_id,
      'help[chat_telegram_id]': message_chat_id.to_s,
      'help[telegram_time]': message.date,
      'help[country]': self.country,
      'help[first_name]': message.from.first_name,
      'help[last_name]': message.from.last_name,
      'help[username]': message.from.username,
    }
  end

  def get_admin_help
    url = event_bot_help_url(:admin)
    params = info_params
    info_request(url, params)
  end

  def help_command?(message_text)
    ['/help', 'help', '/help', '/допомога', '/Допомога'].include?(message_text)
  end

  def help_command(message_text)
    begin
      self.get_help
    rescue
      'Bot help'
    end
  end

  def get_help
    url = event_bot_help_url(:commands)
    params = info_params
    info_request(url, params)
  end


  def help_ru_command?(message_text)
    ['/help-ru', 'helpru', '/helpru', '/помощь', '/Помощь'].include?(message_text)
  end

  def help_ru_command(message_text)
    begin
      self.get_help_ru
    rescue
      'Bot help ru'
    end
  end

  def get_help_ru
    url = event_bot_help_url(:commands_ru)
    params = info_params
    info_request(url, params)
  end

  def start_command?(message_text)
    ['/start', '/старт', '/Старт'].include?(message_text)
  end

  def start_command(message_text)
    answer_text = nil
    self.log("Start tracking for #{ message_from.inspect }")
    if message_from.is_bot
    elsif ignore_command?
      answer_text = IGNORE_COMMAND_TEXT
    else
      default_answer_text = "Вітаю! Почнемо реєстрацію #{ format_name message_from.first_name } #{ format_name message_from.last_name }"
      answer_text = self.start rescue default_answer_text
=begin
      begin
        answer_text += "\n" + (self.get_start_help || '')
      rescue => exception
        handle_exception(exception)
      rescue
        answer_text += "\n" + HELP_TEXT
      end
=end
    end
    answer_text
  end

  def get_start_help
    url = event_bot_help_url(:start)
    params = info_params
    info_request(url, params)
  end

  def about_command?(message_text)
    ['/about', '/info', '/information', '/інформація', '/информация', '/інфо', '/инфо', 'about', 'info', 'information', 'інфо', 'інформація', 'информация', 'инфо'].include?(message_text)
  end

  def about_command(message_text)
    self.get_about || MAIN_CHAT_NAME
  end

  def get_about
    url = event_bot_help_url(:event2)
    params = info_params
    info_request(url, params)
  end

  def interesting_command?(message_text)
    ['/interesting', 'interesting', '/цікаве', '/корисне', 'цікаве', 'корисне'].include?(message_text)
  end

  def interesting_command(message_text)
    self.get_interesting || MAIN_CHAT_NAME
  end

  def get_interesting
    url = event_bot_help_url(:interesting)
    params = info_params
    info_request(url, params)
  end

  def event_bot_help_url(action_name)
    "event_help/#{ action_name }"
  end

  def info_request(url, params, debug=false)
    format = '.html'
    uri = URI(REGISTER_API_URL + url + format)
    uri.query = URI.encode_www_form(params)

    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      log(res.body.strip) if debug
      res.body.strip
    end
  end

  def start
    register_user rescue ''
  end

  def forward_from_chat?(chat_id)
    false
  end

  def forward_by_message_text?(message_text)
    message_text && message_text.to_s.start_with?('/forward')
  end

  def forward_message?(message_text)
    begin
      forward_from_chat?(message_chat_id) || forward_by_message_text?(message_text) 
    rescue => e
      handle_exception(e)
      false
    end
  end

  def forward_message_to(chat_id)
    log "Forward message #{telegram_message_id} to #{chat_id}"
    bot.api.forwardMessage({
      chat_id: chat_id,
      from_chat_id: message_chat_id,
      message_id: telegram_message_id,
      text: 'Message forwarded'
    })
  end

  def forward_message()
    if self.bot_admin?
      forward_message_to(BOT_ADMIN)
    else
      #forward_message_to(telegram_sender_id)
    end
  end

  def history_chat_id
    if self.test_bot
      TEST_VIKING_BOT_GROUP_NEW_ID
    else
      UKRAINE_RUNNING_EVENTS_HISTORY_ID
    end
  end

  def track_message_in_history(message_text, try_index=0)
    forward_message_to(history_chat_id)
    track_message_result = get_track_message
    @tracked_message_ids = track_message_result.to_s.split("|aftermessageids|").first
    track_message_notice = track_message_result.to_s.split("|aftermessageids|").last || 'Message registered'
    puts "tracked_message_ids: #{ tracked_message_ids }"
    if try_index > 0
      puts 'max tries reached'
    elsif tracked_message_ids.nil? || tracked_message_ids.empty?
      start_command(message_text)
      track_message_in_history(message_text, try_index+1)
      send_track_message_notice(track_message_notice)
    end
  end

  def send_track_message_notice(track_message_notice)
    self.send_message(
      parse_mode: 'Markdown',
      chat_id: UKRAINE_RUNNING_EVENTS_HISTORY_ID,
      text: track_message_notice
    )
  end

  def get_track_message
    puts "get_track_message"
    url = event_bot_messages_url(:register)
    params = track_message_params
    info_request(url, params)
  end

  def event_bot_messages_url(action_name)
    "event_bot_messages/#{ action_name }"
  end

  def telegram_message_id(telegram_message=nil)
    telegram_message ||= message
    telegram_message.message_id
  end

  def telegram_sender_id(telegram_message=nil)
    telegram_message ||= message
    UkraineRunningEventsParser.telegram_sender_id(telegram_message)
  end

  def self.telegram_sender_id(telegram_message=nil)
    telegram_message.from.id
  end

  def track_message_params
    params_value = {
      'event_bot_message[telegram_id]': telegram_sender_id,
      'event_bot_message[message]': message.text || message.caption,
      'event_bot_message[telegram_message_id]': telegram_message_id,
    }
    if message.photo && message.photo && !message.photo.empty?
      message.photo.each_with_index do |photo, photo_index|
        photo_file = bot.api.get_file(file_id: photo.file_id)
        puts photo_file.inspect
        photo_file_url = telegram_photo_file_link(photo_file)
        #params_value["event_bot_message[file_id][#{photo_index}]"] = photo.file_id
        params_value["event_bot_message[filename][#{photo_index}]"] = photo_file_url if photo_file_url
      end
    end
    puts 'params_value: ' + params_value.inspect
    params_value
  end

  def telegram_photo_link(photo)
    telegram_file_link(photo.file_id)
  end

  def telegram_file_link(file_id)
    [
      'https://api.telegram.org/bot',
      api_key,
      '/getFile?file_id=',
      file_id
    ].join('')
  end

  def telegram_photo_file_link(photo_file)
    puts "telegram_photo_file_link(#{photo_file.inspect})"
    if photo_file['result'] && photo_file['result']['file_path']
      [
        'https://api.telegram.org/file/bot',
        api_key,
        '/',
        photo_file['result']['file_path']
      ].join('')
    end
  end

  def message_photo_location(photo, photo_index)
    puts photo.inspect
    Dir.mkdir(PHOTO_DIR_NAME) unless Dir.exists?(PHOTO_DIR_NAME)
    "#{ PHOTO_DIR_NAME }/ user_#{ telegram_sender_id }_message_#{ telegram_message_id }_#{ photo_index }"
  end

  def bot_name
    'UkraineRunningEventsBot'
  end

  def bot_name_reference
    ('@'+bot_name).downcase
  end

  def unsupported_symbols
    [
      '"',
      DEBUG_KEYWORD,
      FULLTEXT_KEYWORD,
      'km',
      'км',
    ]
  end

  def supported_countries
    [
      UKRAINE_CODE,
      BILORUS_CODE,
      RUSSIA_CODE,
    ]
  end

  def skip_message?(_message=nil)
    _message ||= message
    UkraineRunningEventsParser.skip_message?(_message)
  end

  def self.skip_message?(message)
    message.nil? || message.is_a?(Telegram::Bot::Types::Poll)
  end

  def ignore_command?
    !bot_admins? && (skip_message? || !individual_chat_message?)
  end

  def replyKeyboardMarkup(answer_to_send)
    Telegram::Bot::Types::ReplyKeyboardMarkup.new({
      keyboard: keyboard_buttons(answer_to_send)
    })
  end

  def click_text
    "НАТИСНИ 👉"
  end

  def button_captions_from_text(answer_to_send)
    captions_array = answer_to_send.
      split("\n").
      select{ |answer_line| answer_line.include?(click_text) }.
      map{ |answer_line| answer_line.split(click_text).last }.
      map{ |button_text| humanize_button_text(button_text.strip.downcase) }
    @keyboard_exists = !captions_array.empty?
    captions_array
  end

  def only_command_without_options(button_text)
    if button_text.include?(' ')
      button_text.split(' ').first
    else
      button_text
    end
  end

  def humanize_button_text(button_text)
    @button_captions ||= {}
    @button_captions[ only_command_without_options(button_text) ] || button_text
  end

  def register_button_caption_replacements(replacements_text)
    log "\n#{'='*20}REPLACEMENTS-TEXT-START#{'='*20}\n\n"
    @button_captions ||= {}
    replacements_text.split( EVENT_COMMANDS_REPLACEMENTS_ROW_SEPARATOR ).each_with_index do |rule, rule_index|
      log "#{ rule_index }: parse #{ rule.inspect }"
      rule = rule.to_s.strip
      if rule.empty?
      elsif rule.start_with?('Prompts::')
      else
        rule_parts = rule.split( EVENT_COMMANDS_REPLACEMENTS_RULE_SEPARATOR )
        if rule_parts && (rule_parts.length > 1)
          rule_key = rule_parts.first.strip.downcase
          rule_key = rule_key[1..-1] if rule_key.start_with?('/')
          rule_key = rule_key.split(' ').first if rule_key.include?(' ')
          rule_value = rule_parts.last.strip
          @button_captions['/' + rule_key] = rule_value
          @button_captions[''  + rule_key] = rule_value
          log "#{ rule_index }: #{ rule_key } => #{ rule_value }"
        end
      end
    end
    log "\n#{'='*21}REPLACEMENTS-TEXT-END#{'='*21}\n\n"
  end

  def encode_text(answer_to_send)
    if answer_to_send
      answer_to_send.force_encoding( MESSAGE_ENCODING_NAME ).encode
    else
      ''
    end
  end

  def keyboard_buttons(answer_to_send)
    button_captions_from_text(answer_to_send).
      map{|button_caption| keyboard_button(button_caption) }
  end

  def keyboard_button(button_caption)
    Telegram::Bot::Types::KeyboardButton.new({
      text: button_caption,
    })
  end

  def send_message(options={})
    log("bot.api.send_message: #{options.inspect}")
    bot.api.send_message(options)
  end

  def send_document(options={})
    log("send_document: #{options.inspect}")
    bot.api.send_document(options)
  end

  def added_new_members?
    begin
      message.new_chat_members && !message.new_chat_members.empty?
    rescue => e
      handle_exception(e)
      false
    end
  end

  def find_chat?(message_text)
  end

  def find_chat(message_text)
  end

  def find_user?(message_text)
  end

  def find_user(message_text)
  end

  def find_run?(message_text)
  end

  def find_run(message_text)
  end

  def handle_exception(e, message_rows=nil)
    error_rows = []
    error_rows << "Time: #{Time.now}"
    error_rows << "Error: #{e.message}"
    error_rows << "Message: #{self.message.inspect}"
    error_rows << "#{self.message_inspect}"
    if message_rows
      message_rows.each_pair do |message_key, message_row|
        error_rows << "#{message_key}: #{message_row}"
      end
    end
    error_rows << "Backtrace: \n#{e.backtrace.join("\n")}"
    write_rows_to_error_log(error_rows)
    write_rows_to_console(error_rows) if test_bot
  end

  def self.handle_exception(e, message_rows=nil)
    error_rows = []
    error_rows << "Time: #{Time.now}"
    error_rows << "Error: #{e.message}"
    if message_rows
      message_rows.each_pair do |message_key, message_row|
        error_rows << "#{message_key}: #{message_row}"
      end
    end
    error_rows << "Backtrace: \n#{e.backtrace.join("\n")}"
    write_rows_to_error_log(error_rows) unless skip_exception?(e)
  end

  def write_rows_to_error_log(error_rows)
    UkraineRunningEventsParser.write_rows_to_error_log(error_rows)
  end

  def write_rows_to_console(error_rows)
    UkraineRunningEventsParser.write_rows_to_console(error_rows)
  end

  def self.new_log_filename
    log_record_time = Time.now
    filename = "log/error-#{log_record_time.to_i}-#{ log_record_time.strftime '%Y%m%d%H%M%S' }.log"
  end

  def self.write_rows_to_console(error_rows)
    if error_rows
      error_rows.each do |error_row|
        puts("#{error_row}\n")
      end
    end
  end

  def skip_exception?(e, message_rows=nil)
    UkraineRunningEventsParser.skip_exception?(e)
  end

  def self.skip_exception?(e, message_rows=nil)
    e.message.to_s.include?('Failed to open TCP connection to api.telegram.org:443 (getaddrinfo: Name or service not known)')
  end

  def self.write_rows_to_error_log(error_rows)
    File.open(UkraineRunningEventsParser.new_log_filename, 'w') do |f|
      if error_rows
        error_rows.each do |error_row|
          f.write("#{error_row}\n")
        end
      end
    end
  end

  def say_hello_text(user_names)
    [
      "Привіт, #{format_name user_names.join(', ')}!",
      "Для початку роботи з ботом напиши /start",
      "І запитання нашого чату:",
      "Що для #{(user_names.count > 1) ? 'Вас' : 'тебе'} біг?"
    ].join("\n")
  end

  def new_members_names()
    message.new_chat_members.map do |new_member_user|
      name = [new_member_user.first_name, new_member_user.last_name].join(' ').strip
      if name.empty?
        name = new_member_user.username
      else
        name
      end
    end
  end

  def preprocess_message
    message_text = message.text
    if message_text
      unsupported_symbols.each do |symbol|
        if message_text.include?(symbol)
          message_text = message_text.split(symbol).join(' ')
        end
      end
      message_text = message_text.to_s.downcase.strip
      if message_text.include?(ID_KEYWORD)
        self.withid = true
        message_text = message_text.split(ID_KEYWORD).join(' ').strip
      end
      supported_countries.each do |country_symbol|
        if !self.country && message_text.end_with?(country_symbol)
          self.country = country_symbol
          message_text = message_text[0..-3].strip
        end
      end
      YESTERDAY_KEYWORDS.each do |yesterday_symbol|
        if !self.date && message_text.include?(yesterday_symbol) && !message_text.start_with?(yesterday_symbol)
          self.date = :yesterday
          message_text = message_text.split(yesterday_symbol).join(' ').strip
        end
      end
      TODAY_KEYWORDS.each do |today_symbol|
        if !self.date && message_text.include?(today_symbol) && !message_text.start_with?(today_symbol)
          self.date = :today
          message_text = message_text.split(today_symbol).join(' ').strip
        end
      end
      if message_text.end_with?(bot_name_reference)
        message_text = message_text[0..-19]
      elsif message_text.include?(bot_name_reference)
        message_text = message_text.split(bot_name_reference).join('')
      end
    else
      message_text = ''
    end
    message_text
  end

  def admin_command?(message_text)
    message_text == 'admin?'
  end

  def admin_command(message_text)
  end

  def long_message?(message_text)
    message_text && (message_text.length > 100)
  end

  def individual_chat_message?
    telegram_sender_id == message_chat_id
  end

  def long_message(message_text)
    if forward_message?(message_text)
      log FORWARD_MESSAGE_TEXT
      self.forward_message()
    end
    if !message.from.is_bot && individual_chat_message?
      UNKNOWN_COMMAND_TEXT + "\n" + HELP_TEXT
    else
      log SKIP_LONG_MESSAGE
    end
  end

  def forward_message_if_need(message_text)
    if forward_message?(message_text)
      log ['else', FORWARD_MESSAGE_TEXT].join(' ')
      self.forward_message()
    end
  end

  def wait_for_command?
    false
  end

  def other_messages(message_text)
    answer_text = nil
    if !message.nil? && !message.from.is_bot && individual_chat_message? && wait_for_command?
      begin
        #log('public_methods: ' + message.public_methods.inspect)
      rescue => e
      end
      if !message.nil? && !message.from.is_bot && individual_chat_message?
        answer_text = UNKNOWN_COMMAND_TEXT + "\n" + HELP_TEXT
      elsif self.bot_admin?
        log OTHER_MESSAGE_TEXT
      else
        log OTHER_MESSAGE_TEXT
      end
    else
      answer_text = other_message_request
    end
  end

  def other_message_request
    puts :other_message_request
    url = event_bot_messages_url(:other_message)
    params = other_message_params
    info_request(url, params)
  end  

  def other_message_params
    params_value = {
      'event_bot_message[telegram_id]': telegram_sender_id,
      'event_bot_message[message_ids]': tracked_message_ids.to_s.strip,
      'event_bot_message[telegram_message_id]': telegram_message_id,
    }
    puts 'other_message_params: ' + params_value.inspect
    params_value
  end

  def get_answer_text
    message_text = preprocess_message
    answer_text = nil
    track_message_in_history(message_text) if !skip_message? && individual_chat_message?
    if skip_message?
    elsif long_message?(message_text)
      answer_text = long_message(message_text)
    elsif start_command?(message_text)
      answer_text = start_command(message_text)
    elsif help_ru_command?(message_text)
      answer_text = help_ru_command(message_text)
    elsif help_command?(message_text)
      answer_text = help_command(message_text)
    elsif about_command?(message_text)
      answer_text = about_command(message_text)

    elsif self.find_user?(message_text)
      answer_text = self.find_user(message_text)
    elsif self.bot_admin_help?(message_text)
      answer_text = bot_admin_help(message_text)
    elsif self.send_urorv2?(message_text)
      send_urorv2
    elsif admin_command?(message_text)
      answer_text = admin_command(message_text)
    else
      forward_message_if_need(message_text)
      answer_text = other_messages(message_text)
    end
    answer_text
  end

  def debug_answer_text(answer_text)
    if message.text && message.text.include?(DEBUG_KEYWORD)
      log answer_text
    elsif answer_text.size > MESSAGE_TEXT_IN_LOG_SIZE
      log "#{answer_text[0..(MESSAGE_TEXT_IN_LOG_SIZE-1)]}..."
    else
      log answer_text
    end
  end

  def admin_comands
    [
      '/ListRegisteredParticipants',
      '/WaitingForConfirmationParticipants',
    ].compact
  end

  def is_admin_command?
    admin_comands.include?(message.text)
  end

  def send_answer_only_text_full_or_by_partial(answer_text)
    puts "send_answer_only_text_full_or_by_partial #{answer_text.to_s.size}"
    answer_text = answer_text.to_s.strip
    begin
      max_message_length = TELEGRAM_MAX_MESSAGE_TEXT_SIZE
      puts "send_answer_only_text_full_or_by_partial max_message_length #{max_message_length}"
      if !answer_text.empty?
        puts "answer_text not empty"
        debug_answer_text(answer_text)
        if answer_text.size < max_message_length
          send_answer_text_full(answer_text)
        elsif message.text.include?(FULLTEXT_KEYWORD) || is_admin_command?
          send_answer_text_by_partial(answer_text, max_message_length)
        else
          send_answer_text_with_limit(answer_text, max_message_length)
        end
      end
    rescue => e
      self.handle_exception(e, answer: answer_text)
    end
  end

  def send_answer_text_by_partial(answer_text, max_message_length)
    log "send_answer_text_by_partial #{answer_text.to_s.size}"
    current_message_index = 0
    while(current_message_index < answer_text.size)do
      last_index = max_message_length-1
      global_last_index = current_message_index+last_index
      send_message_text = answer_text[current_message_index..global_last_index]
      unless send_message_text.rindex(LAST_CHARACTERS_IN_LONG_MESSAGE)
        last_index = send_message_text.rindex(END_ROW_CHARACTERS_IN_LONG_MESSAGE)
        last_index = (last_index || 0) + END_ROW_CHARACTERS_IN_LONG_MESSAGE_SHIFT
        global_last_index = current_message_index+last_index
        send_message_text = answer_text[current_message_index..global_last_index]
      end
      begin
        partial_name = "partial #{ current_message_index }..#{ global_last_index }"
        send_answer_message(send_message_text.force_encoding(MESSAGE_ENCODING_NAME))
        log 'SUCCESS'
      rescue => e
        partial_error = [partial_name, 'ERROR', e.message, "\n"].join(' ')
        self.handle_exception(e, error: partial_error, message_answer: send_message_text.inspect)
      end
      current_message_index += last_index+1+2
      sleep(0.1) # wait 0.1 second and send next part of answer
    end
  end

  def send_answer_text_with_limit(answer_text, max_message_length)
    last_index = max_message_length-1
    send_message_text = answer_text[0..last_index]
    unless send_message_text.rindex(LAST_CHARACTERS_IN_LONG_MESSAGE)
      last_index = send_message_text.rindex(END_ROW_CHARACTERS_IN_LONG_MESSAGE)+END_ROW_CHARACTERS_IN_LONG_MESSAGE_SHIFT
      send_message_text = answer_text[0..last_index]
    end
    send_answer_message(send_message_text.force_encoding(MESSAGE_ENCODING_NAME))
  end

  def send_answer_text_full(answer_text)
    puts "send_answer_text_full #{answer_text.to_s.size}"
    send_answer_message(answer_text)
  end

  def message_options(answer_to_send)
    {
      reply_to_message_id: telegram_message_id,
      parse_mode: 'Markdown',
      chat_id: telegram_sender_id,
      text: answer_to_send
    }
  end

  def status_button_text
    'status'
  end

  def keyboard_options(answer_to_send)
    @keyboard_options = {
      resize_keyboard: true,
      one_time_keyboard: true,
      reply_markup: replyKeyboardMarkup(answer_to_send),
    }
    if @keyboard_exists
      @keyboard_options
    elsif status_button_text
      {
        resize_keyboard: true,
        one_time_keyboard: true,
        reply_markup: replyKeyboardMarkup(click_text + status_button_text),
      }
    else
      puts "NO KEYS IN KEYBOARD"
      {}
    end
  end

  def send_answer_message(answer_to_send)
    begin
      self.send_message(
        message_options(answer_to_send).
        merge(keyboard_options(answer_to_send))
      )
    rescue => e
      answer_text = MESSAGE_SENDING_ERROR
      self.handle_exception(e, answer: answer_text, message_answer: answer_to_send.inspect)
    end
  end

  def perform
    answer_text = nil
    begin
      answer_text = self.get_answer_text
    rescue => e
      if self.bot_admin?
        answer_text = MESSAGE_PROCESSING_ERROR
      end
      self.handle_exception(e)
      raise e
    end
    self.send_encoded_answer(answer_text)
  end

  def anly_allowed_symbols(text)
    text.to_s.split('_').join('-')
  end

  def send_encoded_answer(answer_text)
    self.send_answer_with_images_and_replacements(encode_text(answer_text))
  end

  def send_answer_with_images_and_replacements(answer_text)
    puts "send_answer_with_images_and_replacements #{answer_text.to_s.length}"
    answer_with_replacements_partials = answer_text.split( EVENT_COMMANDS_REPLACEMENTS_TEXT_SEPARATOR )
    if answer_with_replacements_partials.size > 1
      register_button_caption_replacements( (answer_with_replacements_partials.last || '').strip )
    end
    self.send_answer_with_images( (answer_with_replacements_partials.first || '').strip )
  end

  def send_answer_with_images(answer_text)
    puts "send_answer_with_images #{answer_text.to_s.length}"
    if answer_text
      if answer_text.index( IMAGE_LIST_START )
        answer_text.split(IMAGE_LIST_START).each_with_index do |answer_part, answer_part_index|
          unless answer_part_index.zero?
            image_text_parts = answer_part.split( IMAGE_LIST_END )
            self.send_images_from_text_answer( image_text_parts.first )
            self.send_answer_only_text_full_or_by_partial(anly_allowed_symbols image_text_parts.last ) if image_text_parts.size > 1
          else
            self.send_answer_only_text_full_or_by_partial(anly_allowed_symbols answer_part)
          end
        end
      else
        self.send_answer_only_text_full_or_by_partial(anly_allowed_symbols answer_text)
      end
    end
  end

  def send_images_from_text_answer(images_in_text)
    log "send_images_from_text_answer(#{images_in_text})"
    images_in_text.to_s.split( IMAGE_LIST_ITEM ).each_with_index do |image_in_text, image_index|
      self.send_image_from_text_answer( "image:#{ image_index }\n#{ image_in_text }" )
    end
  end

  def debug_sending_images?
    false
  end

  def send_image_from_text_answer(image_in_text)
    log "send_image_from_text_answer(#{image_in_text})"
    if image_in_text && !image_in_text.empty?
      image_parts = image_in_text.split("\n")
      image_file_index = image_parts[0]
      image_file_name = image_parts[2]
      image_file_path = image_parts[3]
      image_file_size = image_parts[4]
      image_file_caption = anly_allowed_symbols image_parts[5]
      if debug_sending_images?
        self.send_answer_text_full([
          "index: #{image_file_index}",
          "name: #{image_file_name}",
          "path: #{image_file_path}",
          "size: #{image_file_size}",
          "caption: #{image_file_caption}",
        ].join("\n").split('_').join('-'))
      end
      send_photo(image_file_name, image_file_path, image_file_caption)
    end
  end

  def send_urorv2?(message_text)
    message_text.include?('sendurorv2') && self.bot_admin?
  end

  def send_urorv2
    UkraineRunningEventsParser.list_of_users.each_with_index do |r, r_i|
      begin
        self.send_message(
          parse_mode: 'Markdown',
          chat_id: r.first,
          text: UkraineRunningEventsParser.congratulation_online_run_vyshyvanka(r[1])
        )
        puts "SENT #{r_i}"
      rescue => e 
      end
    end
  end

  def self.list_of_users
    [["475157671", "Olena Stepanchuk"], ["461043116", "Oleksandr Slipets"], ["1051128662", "Artem Dudko"], ["347606588", "Sergey Alekseenko"], ["445809264", "Vlad Klyuchuk"], ["359174325", "Ihor Hrabovskyi"], ["396213925", "Andrii"], ["724945395", "Artem Bukhalo"], ["749789545", "Yurii"], ["560568670", "DimaSerd"], ["422503390", "Ольга"], ["911049143", "Наталія Романюк"], ["739651078", "Богдан Богдан"], ["372932402", "Serg Vinichenko"], ["478257420", "Andrii Kyrylenko"], ["613280827", "Ansar Ansar"], ["901135340", "Аліна Білецька"], ["845911102", "Олеся Зятик"], ["821992396", "Dmytro Davydenko"], ["572781999", "Александр Горбашов"], ["763002121", "Сергій Невмирич"], ["590797662", "Volodymyr Butenko"], ["715702872", "Михайло Проскурня"], ["478790557", "МаксБобр"], ["1181413499", "Николай Маценко"], ["694531228", "Valentyna )"], ["158255177", "Ruslan Y. #РазумныйМаркетолог"], ["271206883", "Volodymyr Kashchuk🇺🇦"], ["812598546", "Антон Мишарин"], ["505327947", "Deva"], ["968861977", "Андрій Коваленко"], ["531608345", "Ольга Лукьянчук"], ["652830747", "Наталія Гудзенко"], ["960908274", "Stanislav Yerko"], ["797192457", "Aleksandr Tertychenko"], ["557883166", "Юра Федорович"], ["470552849", "Yelyzaveta Petrusenko"], ["413951392", "Lada K"], ["525632559", "Natali"], ["840368013", "Victoria Kaminska"], ["1099614221", "Tanya Gerashchenko"], ["680315260", "Евгения Сорокина"], ["740830660", "Ekaterina Romanova"], ["463277637", "Anastasiia Sansick"], ["737998819", "Я"], ["569397862", "Paul Smith"], ["286782056", "Tetiana Synytsyna"], ["237201856", "Kateryna Sokolova"], ["292986424", "Andrii Kositsyn"], ["473208513", "Yarik 009"], ["485223088", "Александр Кобельчук"], ["560633249", "Oksana"], ["442773598", "Iryna Orlova"], ["902803478", "Ольга"], ["1079544332", "Йосип Шевчук"], ["355720437", "Сергій Опалко"], ["774424556", "Елан"], ["684771124", "Vova Bamburak"], ["400401155", "Филипп"], ["563155676", "Курочкина Лилия"], ["779620628", "Андрій Трохимчук 🇺🇦"], ["584254380", "Ксеня"], ["813862040", "Антонина Тарабанова"], ["85633853", "Олександр Діхтяренко"], ["952634721", "Анна Трунова"], ["470980520", "Maryna"], ["898230901", "Victoria"], ["655751152", "Черевична Людмила"], ["1232049688", "Міша Міша"], ["449155275", "Oleksandr Shepetiuk"], ["802335798", "Максим Гаврилів"], ["577593592", "Tatyana Bodnar"], ["450217966", "Daria Skoromets"], ["1070997439", "Варвара"], ["492914416", "Дмитрий Мурадов"], ["771143455", "Ivanka Demkiv"], ["790535524", "Илья Демидов"], ["743482430", "Алёна"], ["1107456501", "Alexey Topchii"], ["793247565", "Елена Еремина"], ["151164632", "Kasianenko Iryna"], ["332122157", "BorysB Borys"], ["456257930", "Роман Есин"], ["345940178", "Fedor Nishchimenko"], ["396078141", "Леонид Величко"], ["1092580970", "Viacheslav Iefimtsev"], ["1225860488", "Василь Макогін"], ["944347939", "Марина Плетюк"], ["412917497", "Nata"], ["961935488", "Lidiya"], ["791341406", "Дарья Кожемякина"], ["454159950", "39445 Сергей"], ["1183329616", "Анастасія Левенцова"], ["874723259", "Светлана"], ["150656611", "Taras Kozak"], ["987800201", "Надія Дрозд"], ["541362433", "Mariam Shevchenko"], ["1011267418", "Олександр Олександр"], ["455689053", "Віктор Підгрушний"], ["372545776", "Tani"], ["694366836", "Ірина"], ["372569209", "Elis"], ["567426949", "Nik Julia"], ["1244611841", "Oleg Rimarovich"], ["387329332", "Женя Яценко"], ["572964695", "Vadim Vadim"], ["798197930", "Марина Розуменко"], ["163819162", "Bogdan Znakharenko"], ["411824484", "Виктория Капустян"], ["775234884", "Арианна Арианна"], ["531837958", "Леся Мельничук"], ["500841034", "Людмила Михайлова"], ["724386544", "Ирина Шевчук"], ["207050188", "Yevhen Moroz"], ["786687674", "Наталія"], ["607979422", "Софія Крилова"], ["268704234", "Nikolay Karlov"], ["486485467", "Дарія"], ["796634089", "Александр Царёв"], ["616645984", "Юля Жакун"], ["886230001", "Олег"], ["987541861", "Виктор Ярмола"], ["416680175", "Anastasiia"], ["388855608", "Yulia Kuznetsova"], ["485344831", "Mariia Zaiats"], ["665681934", "Дарія Поддубецька"], ["405578210", "Svetlana Vasylenko"], ["891028161", "27949 Марина"], ["405902937", "bondar jr."], ["64123293", "DIMA"], ["140034240", "Alena Filatova"], ["613328007", "Olya Yurenko"], ["394117059", "Vetas Vetas"], ["512719031", "Сергий Фабриков"], ["1112541125", "Павло Левчук"], ["939805990", "МАРИНА"], ["1198060646", "Вишневська Ксенія"], ["670263926", "Юрій"], ["505312753", "Luba Mova"], ["407246900", "Angelina"], ["1149721085", "Наташа Римарович"], ["851796158", "Олександр Войналович"], ["456077953", "Оксана"], ["333599263", "Ігор Патіюк"], ["381228777", "Svitlana"], ["137220734", "Evgen"], ["518470133", "Pavlo Postnikov"], ["592979358", "Анастасия"], ["455916708", "Anna Kachykova"], ["171090386", "Sergei"], ["697817006", "Вєроніка Вєроніка"], ["1140728100", "Сергей Немцов"], ["981866544", "Юрий Зарудный"], ["486798440", "Альона"], ["1023433999", "Наталія Лойко"], ["409216194", "Ruslana"], ["520344972", "Алексей Мамонт"], ["571684719", "Oleh Bukatar"], ["753254471", "Ruslan Kolesnik"], ["966143079", "Miros Pauk"], ["948377529", "Yana"], ["543979428", "Iuliia Fursenko"], ["880124423", "Andriy Ehrlich"], ["861351984", "Юлия Юлия"], ["271758603", "Марина Железкина"], ["382286149", "Вадим"], ["665739994", "Алеся"], ["408875742", "Andrii Koliadenko"], ["90475657", "Elena"], ["357739767", "Hlib Stryzhko"], ["764459542", "alyona demyanenko"], ["488794597", "олег русалим"], ["898138468", "Artem Zavhorodnii"], ["1015360371", "Ольга Фетисова"], ["1126823144", "Олег Завальницький"], ["479088202", "Сергій Левчик"], ["501280264", "Olga Cherkes"], ["1088676438", "Юлія Радецька"], ["413070256", "Валерий Кузнецов"], ["840904422", "Настя"], ["618664116", "Ірина"], ["1114249135", "Толік Дитина"], ["977203776", "Таня Белецкая"], ["445971828", "Mirona Zulgarina"], ["589881227", "Миськив Нелли"], ["469419600", "Кирилл Дубовик"], ["740497689", "Olha Modina"], ["450589218", "Сергей"], ["887299309", "Nataliia Serhiienko"], ["450193895", "Сергей"], ["445485389", "Сергей Лозовский"], ["1092052070", "Дмитрий Васильцов"], ["916421318", "Віталій Хорішко"], ["1092023696", "Оксана Михайлова"], ["931866417", "Валентина Борульник"], ["353302503", "Tanya Trokhymets"], ["1297822972", "Тетяна Чернюк"], ["553711051", "Никита Литвиненко"], ["777319527", "Удовенко Татьяна"], ["600775555", "Елена Саволикова"], ["279379643", "Andry Hevedze"], ["769045075", "Artur Haiduk"], ["464498295", "Сергій"], ["793963649", "Олена"], ["772908574", "Svitlana☀️"], ["593806193", "dmytro"], ["306737719", "Oleksandr Lytovchenko"], ["411945924", "Юлия"], ["536587355", "🌠🌠🌠"], ["735428155", "Pavlo Derkach"], ["442754890", "Сергій Борков"], ["992644394", "Liana Kopulova"], ["616376041", "Aleksandr Artyshuk"], ["960384423", "Elena Vyhuliar"], ["505234034", "Anna"], ["873262294", "Георгій Фечо"], ["725696736", "Dina"], ["1070056250", "Лариса Борсук"], ["332423132", "Andriy Didok"], ["452385426", "Kse Belka 🐿"], ["702908573", "Zanna Z"], ["1299284748", "Олена Кривощокова"], ["530307213", "Maxim Alekseenko"], ["1234930705", "Зоряна Зеліско"], ["588278204", "ЮРІЙ ЛОЗЕНКО"], ["671333992", "Вікторія"], ["341138886", "Сергей"], ["261020007", "Evgeniya Gaiduk"], ["794134986", "Балак Балак"], ["335092093", "Olga Sardak"], ["671461486", "Anna Hontaruk"], ["533474924", "Андрей"], ["787248996", "Оля Гуль"], ["984492614", "Irina Baranova"], ["924876623", "Роман Роман"], ["424260923", "Nadyusha Kozak"], ["455697167", "Viktoria"], ["974481086", "Іван Макарук"], ["338222048", "Леонід Тищук"], ["551169176", "Anna Tolstaia"], ["441542908", "Igor"], ["437195781", "Tatiana Vyaznikova"], ["561816825", "T. Lesia"], ["818779670", "Max Vor"], ["1028843159", "Александр Яровой"], ["964660743", "Alina"], ["357812722", "Tom"], ["703628166", "Olga"], ["556242819", "Yuliya Yuliya"], ["356438623", "Олександр Василенко"], ["584196382", "Марина"], ["1123685801", "Nataliya"], ["364192812", "Daria Kurennaia"], ["668813823", "Дмитро Горбань"], ["843490100", "Ірина Косович-Онуфрик"], ["833296701", "Ярослав Грицак"], ["579647208", "Інна Миськів"], ["424879196", "ОляМа"], ["432205233", "Nazar Poberezhnyk"], ["440300664", "Sergey Ermakov"], ["929096930", "Potenok Mari"], ["415042645", "Nastiya Rudnytska"], ["558153401", "Oksana Sapiga"], ["866052474", "Dmytro"], ["610285123", "Світлана Шинкаренко"], ["269803335", "Igor Exp."], ["650770740", "Daryna Khodalska"], ["831830473", "Анна Шкредова"], ["513222657", "Olena Kavkun"], ["981254934", "Tatiana Dziuba"], ["754175075", "Aleksandr"], ["708726196", "Marianna"], ["530903005", "Елена Лемешек"], ["649171782", "Buka"], ["995327175", "Светлана"], ["608240888", "Olga"], ["793609397", "lyubomir bg"], ["1033826933", "Eduard"], ["695588837", "Yuliya Misyurak"], ["1150382367", "Таня"], ["380463924", "Аленка Бабич"], ["126269395", "Sergii"], ["554386233", "Ольга Бабич"], ["303822252", "Тарас Шкирта"], ["922352992", "ирина кочеткова"], ["1223803645", "Татьяна Цимбал Татьяна Цимбал"], ["366106795", "Kostyantyn Bylym"], ["414800713", "Валентин"], ["501733991", "Виктор"], ["1197385076", "volodimirurtin@gmail.com marafon2019"], ["192793454", "Стас Дяченко"], ["986036441", "Mykhailo Volkov"], ["974757164", "Denys Roman"], ["277086317", "Sergii Bobryk"], ["818798326", "Сергей GelAmo"], ["180200757", "K.R."], ["428264068", "Vita Lozytska"], ["754463075", "Nikolay Popov"], ["1272687155", "Mаша П"], ["582255210", "Bondarenko Alexey"], ["916585616", "Garik Palokha"], ["539073025", "Dmitro Romashkevich"], ["217780966", "Yulia Korshak"], ["720960582", "Наталі Наталі"], ["547661470", "Sergiy Berezinskiy"], ["355962824", "Ddnk"], ["1055212844", "Лариса Бирюкова"], ["417721851", "Oksana Marusynets"], ["487972685", "Ольга"], ["556485309", "Andrew"], ["712263346", "Світлана Каба"], ["582829461", "Ольга"], ["752522935", "Юра Яровий"], ["1057548488", "Евгений"], ["930223356", "Nataliia Kozlovska"], ["272212110", "Serg Lazar"], ["850118645", "Антон Нещерет"], ["566413090", "Юлія ФЕДОРИШИНА"], ["899154680", "Алена"], ["724918445", "Марія Макарчук"], ["288285308", "Ekaterina Baranova"], ["1112693064", "Eduard"], ["437718995", "Beznavorotov"], ["573718254", "Екатерина Чубарь"], ["474258877", "Natalia K"], ["778516370", "Макс Станиславович"], ["830115647", "Сергій Монайло"], ["468810103", "Kate"], ["849057066", "Ірина"], ["130841803", "Anton Kuts"], ["805546571", "Yuliia Zarvii"], ["395576390", "lena●stukalova"], ["258202175", "Aleksandr Krasnochekov"], ["557936351", "Alex Suporovskiy"], ["506543130", "Оксана"], ["798273532", "Елизавета Миколенко"], ["446318036", "Alina Chorna"], ["615948457", "Roman Maschenko"], ["384496704", "Iryna"], ["801425213", "Анна Меркулова"], ["382887009", "Svetlana"], ["584953435", "Ольга"], ["415539955", "Ann Popova"], ["618485022", "Маринка Семенченко"], ["844220577", "Denis Lebid"], ["790272458", "Yaromenok Maryna"], ["181711257", "Juliya Golovko"], ["1048797049", "Tanya Steel"], ["1022795394", "Alex"], ["711593541", "Натали Сорокина"], ["974981169", "Максим"], ["671298647", "Владимир"], ["507802792", "Anna"], ["693632927", "Anna"], ["677176978", "Артём Моисеенков"], ["950450833", "Ольга Атлетика"], ["899931563", "Liliia Hryb"], ["1165955751", "Yasko Vladimir"], ["349792845", "Yana Shermer"], ["368119944", "Romeo R"], ["1121116330", "Светлана Глебова"], ["612240452", "Denys Horshunov"], ["790834132", "Karapuzyuliya Karapuzyuliya"], ["341313407", "Яна Кузьменко"], ["463309486", "Кирилл"], ["475869033", "Alex"], ["532515825", "Наталья"], ["712103747", "Зоя Рузимурадова"], ["581165317", "Ирина Кэйлян"], ["376852450", "Игорт Горлов"], ["261140310", "Anatoliy Grigoriev"], ["465290015", "Вероника🌷"], ["362092352", "Yury"], ["908863633", "Александра Халеева"], ["1271426545", "Таня Данильченко"], ["143581315", "Roman Yaromenok"], ["108242621", "Running Wolf"], ["537077929", "Anton Harbar (Друк🖨📃)"], ["532541737", "Ірина Кутра"], ["230103999", "Arkadii Borets"], ["532191444", "Gala Rvacheva"], ["1090404146", "Vania"], ["1047811496", "Егор Халеев"], ["446680150", "Nazar Voroniuk"], ["560736834", "Tarik Didenko"], ["286390890", "Влад Рудь"], ["627719191", "Сергей Устинов"], ["472919134", "Tatyana"], ["753703866", "Alyona Golubeva"], ["258090992", "Бєлокуренко Тетяна"], ["792325672", "Irena Kutorgenko"], ["532591128", "Serhii Hotsuliak"], ["455193356", "Evgeniy Rybakov"], ["901917313", "Александр Шевченко"], ["944148234", "Оксанита Мартынюк"], ["846105251", "Ira Koroi"], ["578050742", "Андрій Білик"]]
  end

  def self.congratulation_online_run_vyshyvanka(full_name)
    <<-CONGRATULATION_3RD_ONLINE_RUN
Привіт #{full_name}!

Сьогодні Всесвітній День вишиванки і ми святкуємо його в біговому стилі.
https://www.facebook.com/UkraineRunning/photos/a.633450367000103/1184827615195706
Тому вдягайте свої футболки-вишиванки і гайда на пробіжку, вибирайте дистанцію за бажанням та робіть фотки, пишіть -
#разомбігтти веселіше) з #УкраїнаБіжить та публікуйте їх тут: 
https://www.facebook.com/events/573432220278153/
а кілометри пробіжки у наші чати в телеграм, де бот @UkraineRunningBot їх зафіксує та виведе рейтинг
а ми ж у події переглянемо у кого буде краще фото у футбоці-вишиванці, та по підсумках виберемо тих хто отримує нашу стильну футболку-вишиванку (1-для чоловіка і 1-для жінки)

Всім бажаємо святкового настрою та легких ніг на пробіжках!

З повагою, 
організатори online-забігу -  #УкраїнаБіжить у вишиванках

https://t.me/UkraineRunning; 
Харків - t.me/kharkovrunning; 
Кривий Ріг - goo.gl/JRoVd6; 
Київ - t.me/KyivRunning; 
Херсонщина - t.me/khersonregion;
Миколаїв - t.me/mykolaivrunning;
Одеса - t.me/odessarunning;
Слов'янськ - t.me/Pro_Beg_Slaviansk;
Галичина - t.me/GaliciaRunning;
Запоріжжя - t.me/ZaporizhiaRunning;
Вінниця - t.me/VinnitsiaRunning;
Дніпро - t.me/DneprRunning;
Краматорськ - t.me/KramRun;
Kryvyi Rig Ukraine Run - t.me/KRURun;
Чернігів - t.me/ChernihivRunning;
Чат сили (Київ) - t.me/chatsily;
Житомир - t.me/ZhytomyrRunning;
Луганщина - t.me/LuganskRegionRunning;
Маріуполь - t.me/mariupolrunning;
Черкаси - t.me/CherkasyRunning;
Суми - t.me/SumyRunning;
Running Atmosphere (Кривий Ріг) - t.me/running_atmosphere;
RUN DniproPeremoga - t.me/RUN_DniproPeremoga;
Poltava Running - t.me/PoltavaRunning;
Lviv Running - t.me/lvivrunning;
Волинь біжить - t.me/volynrunning;
ULTRUNNERS - t.me/ultrunners;
Закарпаття біжить - t.me/zakarpattiarunning;
Рівне - t.me/rivnerun2020.
    CONGRATULATION_3RD_ONLINE_RUN
  end

  def self.listen(test_bot=false)
    begin
      Telegram::Bot::Client.run(UkraineRunningEventsParser.bot_token(test_bot)) do |bot|
        log [STARTED_LISTENING_TEXT, (test_bot ? 'TEST' : 'PROD'), MAIN_CHAT_NAME].join(' ')
        bot.listen do |message|
          ukraine_running_parser = UkraineRunningEventsParser.new(message, bot, test_bot)
          ukraine_running_parser.perform
          ukraine_running_parser = nil
        end
      end
    rescue => e
      UkraineRunningEventsParser.handle_exception(e)
      sleep(SLEEP_TIME_IN_SECONDS_AFTER_EXCEPTION)
    end
  end

  def log(message)
    UkraineRunningEventsParser.log(message)
  end

  def self.log(message)
    puts message
  end
end

UkraineRunningEventsParser.log(INITIALIZING_TEXT+' ok')
test_mode_file_exists = File.exists?('test.mode')
while !shutdown_command_received
  UkraineRunningEventsParser.listen(test_mode_file_exists)
end
